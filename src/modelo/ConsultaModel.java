/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author deadbryam
 */
public class ConsultaModel {

    private int id;
    private SalaModel sala;
    private DoctorModel doctor;
    private int telefono;
    private int edad;
    private String sintomas;
    private String paciente;
    private boolean sexo;

    public ConsultaModel() {
    }

    public ConsultaModel(int id,SalaModel sala, DoctorModel doctor, int telefono, int edad, String sintomas, String paciente, boolean sexo) {
        this.id = id;
        this.sala = sala;
        this.doctor = doctor;
        this.telefono = telefono;
        this.edad = edad;
        this.sintomas = sintomas;
        this.paciente = paciente;
        this.sexo = sexo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SalaModel getSala() {
        return sala;
    }

    public void setSala(SalaModel sala) {
        this.sala = sala;
    }

    public DoctorModel getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorModel doctor) {
        this.doctor = doctor;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getSintomas() {
        return sintomas;
    }

    public void setSintomas(String sintomas) {
        this.sintomas = sintomas;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public boolean isSexo() {
        return sexo;
    }

    public void setSexo(boolean sexo) {
        this.sexo = sexo;
    }
    
    public Object[] toArray(){
        return new Object[]{this.id,this.doctor.toString(),this.paciente,this.edad,this.telefono,this.sexo ? "M" : "F",this.sala.toString(),this.sintomas};
    }

}
