/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.DoctorModel;

/**
 *
 * @author deadbryam
 */
public class DoctorController {

    private final Conexion conexion;
    private ResultSet rst;

    public DoctorController() {
        conexion = new Conexion();
    }

    /**
     * Este metodo sirve para convertir un resultSet tipo doctor a un objeto de doctor
     *
     * @param data ResultSet tipo doctor
     * @return Objeto de doctor
     * @throws SQLException
     */
    public DoctorModel resultSetToModel(ResultSet data) throws SQLException {
        return new DoctorModel(data.getString("id_doctor"), data.getString("doctor"));
    }

    /**
     * Este metodo sirve para obtener los doctores de la base de datos
     *
     * @return Un arreglo de modelo de doctor
     */
    public DoctorModel[] getDoctores() {
        List<DoctorModel> resultado = new ArrayList<>();
        rst = conexion.obtener("SELECT * FROM doctor");
        try {
            while (rst.next()) {
                resultado.add(resultSetToModel(rst));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DoctorController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultado.toArray(new DoctorModel[0]);
    }

}
