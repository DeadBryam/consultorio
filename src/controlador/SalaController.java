package controlador;

import controlador.Conexion;
import controlador.DoctorController;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.DoctorModel;
import modelo.SalaModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author deadbryam
 */
public class SalaController {

    private final Conexion conexion;
    private ResultSet rst;

    public SalaController() {
        conexion = new Conexion();
    }

    /**
     * Este metodo sirve para convertir un resultSet tipo sala a un objeto de
     * Sala
     *
     * @param data ResultSet tipo sala
     * @return Objeto de Sala
     * @throws SQLException
     */
    public SalaModel resultSetToModel(ResultSet data) throws SQLException {
        return new SalaModel(data.getInt("id_sala"), data.getString("sala"));
    }

    /**
     * Este metodo sirve para obtener las salas medicas de la base de datos
     *
     * @return Un arreglo de modelo de sala
     */
    public SalaModel[] getSalas() {
        List<SalaModel> resultado = new ArrayList<>();
        rst = conexion.obtener("SELECT * FROM sala");
        try {
            while (rst.next()) {
                resultado.add(resultSetToModel(rst));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DoctorController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultado.toArray(new SalaModel[0]);
    }

}
