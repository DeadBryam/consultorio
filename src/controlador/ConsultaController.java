/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.ConsultaModel;
import modelo.DoctorModel;
import modelo.SalaModel;

/**
 *
 * @author deadbryam
 */
public class ConsultaController {

    private final Conexion conexion;
    private ResultSet rst;

    public ConsultaController() {
        conexion = new Conexion();
    }

    /**
     * Este metodo convierte un resultSet tipo consulta a un objeto de modelo consulta
     * @param set ResultSet tipo consulta
     * @return Objeto de tipo modelo consulta
     * @throws SQLException 
     */
    private ConsultaModel resultSetToModel(ResultSet set) throws SQLException {
        return new ConsultaModel(set.getInt("id"),new SalaModel(set.getInt("id_sala"), set.getString("sala")), new DoctorModel(set.getString("id_doctor"),
                set.getString("doctor")), set.getInt("telefono"), set.getInt("edad"),
                set.getString("sintomas"), set.getString("paciente"), set.getBoolean("sexo"));
    }

    /**
     * Este metodo sirve para agregar una "Consulta" a la base de datos
     *
     * @param model Modelo de consulta
     */
    public void ingresarConsulta(ConsultaModel model) {
        conexion.insertar(String.format("INSERT INTO consulta VALUES(NULL, %d,\"%s\",%d,%d,\"%s\",%b,\"%s\");",
                model.getSala().getId(), model.getDoctor().getId(), model.getTelefono(),
                model.getEdad(), model.getPaciente(), model.isSexo(), model.getSintomas()));
    }

    /**
     * Este metodo sirve para obtener los registros de la tabla consultas
     * @return Una lista de modelo de consulta
     */
    public List<ConsultaModel> obtenerConsultas() {
        rst = conexion.obtener("SELECT * FROM consulta INNER JOIN sala ON sala.id_sala = consulta.id_sala INNER JOIN doctor ON doctor.id_doctor = consulta.id_doctor;");
        List<ConsultaModel> resultado = new ArrayList<>();
        try {
            while (rst.next()) {
                resultado.add(resultSetToModel(rst));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return resultado;
    }

    /**
     * Este metodo elimina un registro por su ID
     * @param id ID de consulta
     */
    public void eliminarRegistro(int id){
        conexion.eliminar("DELETE FROM consulta WHERE id = "+id);
    }
    
    /**
     * Este metodo elimina todos los registros
     */
    public void eliminarTodo(){
        conexion.eliminar("DELETE FROM consulta");
    }
}
